package com.example.android.diagnosisapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity

        implements NavigationView.OnNavigationItemSelectedListener {
    private void showAboutDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppAlertDialog);
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.about, null);

        builder.setCancelable(true)
                .setView(layout);

        final AlertDialog aboutDialog = builder.create();

        android.view.WindowManager.LayoutParams langParams = aboutDialog.getWindow().getAttributes();
        langParams.width = GridLayout.LayoutParams.MATCH_PARENT;
        aboutDialog.getWindow().setAttributes(langParams);

        final TextView view = (TextView) layout.findViewById(R.id.text1);
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mcrops.org"));
                startActivity(browserIntent);
            }

        });


        Button closeBtn = (Button) layout.findViewById(R.id.closeBtn);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                aboutDialog.dismiss();
            }
        });

        aboutDialog.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Disease Classifier");
        toolbar.setSubtitle("www.mcrops.org");




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_diagnosis) {
            // Handle the camera action

        } else if (id == R.id.nav_share) {

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
            i.putExtra(Intent.EXTRA_TEXT, "http://www.air.ug/mcrops");
            startActivity(Intent.createChooser(i, "Share URL"));

        } else if (id == R.id.nav_feedback) {
           // Intent intent = new Intent(Intent.ACTION_SENDTO);
           // intent.setData(Uri.parse("mailto:")); // only email apps should handle this
           // intent.putExtra(Intent.EXTRA_EMAIL, "emwebaze@gmail.com");
            //intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback on your app");
           // if (intent.resolveActivity(getPackageManager()) != null) {
              //  startActivity(intent);
           // }
            final Intent _Intent = new Intent(android.content.Intent.ACTION_SEND);
            _Intent.setType("text/html");
            _Intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.mail_feedback_email)});
            _Intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.mail_feedback_subject));
            _Intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.mail_feedback_message));
            startActivity(Intent.createChooser(_Intent, getString(R.string.title_send_feedback)));

        } else if (id == R.id.nav_about) {
            /*Intent about = new Intent(this, AboutActivity.class);
            startActivity(about);*/
            showAboutDialog();

        } else if (id == R.id.nav_policy) {
            String url = "https://mcrops.org";
            Intent policy = new Intent(Intent.ACTION_VIEW);
            policy.setData(Uri.parse(url));
            startActivity(policy);

        } else if (id == R.id.nav_help) {
            String url = "https://mcrops.org/diseaseclassifier";
            Intent help = new Intent(Intent.ACTION_VIEW);
            help.setData(Uri.parse(url));
            startActivity(help);
            }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
